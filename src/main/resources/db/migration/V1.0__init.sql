create table link
(
    link_id                 bigserial primary key,
    account_id         bigserial not null,
    short_url          varchar not null,
    long_url           varchar not null,
    create_time        timestamp
);

create index if not exists link_account_id_idx on link (account_id);
create index if not exists link_short_url_idx on link (short_url);
create index if not exists link_create_time_idx on link (create_time);