package ru.shurl.storage.utils;

/**
 * Класс с общими константами
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public class Constants {
    public static final String JWT_HEADER = "Authorization";
}
