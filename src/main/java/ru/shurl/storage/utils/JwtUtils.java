package ru.shurl.storage.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.shurl.storage.exceptions.AccessException;
import ru.shurl.storage.model.types.Account;
import ru.shurl.storage.model.enums.UserType;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author vygulyarnyyao on 03.10.2021
 */
@Slf4j
@UtilityClass
public class JwtUtils {

    private static final String ID = "id";
    private static final String TYPE = "type";

    //TODO пока Кирилл не запустил свой генератор токенов юзаем токен вида "id type"
    // например "123 FREE"
    public static Account getAccountByToken(final String jwt) {
        final String[] split = jwt.split(" ");
        return new Account(Long.valueOf(split[0]), UserType.valueOf(split[1]));
    }

//    public static Account getAccountByToken(final String jwt) {
//        try {
//            final String[] split = jwt.split("\\.");
//            final String body = getJson(split[1]);
//            final JSONObject jsonObject = (JSONObject) new JSONParser().parse(body);
//            final long accountId = (Long) jsonObject.get(ID);
//            final UserType userType = UserType.valueOf((String) jsonObject.get(TYPE));
//            return new Account(accountId, userType);
//        } catch (ParseException ex) {
//            log.error("Parse error = {}", ex.getMessage(), ex);
//            throw new AccessException(ex);
//        }
//    }

    private static String getJson(final String strEncoded) {
        final byte[] decodedBytes = Base64.getUrlDecoder().decode(strEncoded);
        return new String(decodedBytes, StandardCharsets.UTF_8);
    }
}
