package ru.shurl.storage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shurl.storage.model.entity.Link;

/**
 * @author vygulyarnyyao on 03.10.2021
 */
public interface LinkRepository extends JpaRepository<Link, Long> {

    Link findByShortUrl(String shortUrl);
}
