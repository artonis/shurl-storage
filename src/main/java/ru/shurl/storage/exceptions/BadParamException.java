package ru.shurl.storage.exceptions;

/**
 * Некорректный параметр
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public class BadParamException extends RuntimeException {
    public BadParamException() {
        super();
    }

    public BadParamException(String message) {
        super(message);
    }

    public BadParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadParamException(Throwable cause) {
        super(cause);
    }
}
