package ru.shurl.storage.exceptions;

/**
 * Ошибка доступа
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public class AccessException extends RuntimeException {
    public AccessException() {
        super();
    }

    public AccessException(String message) {
        super(message);
    }

    public AccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessException(Throwable cause) {
        super(cause);
    }
}
