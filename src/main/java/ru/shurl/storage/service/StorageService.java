package ru.shurl.storage.service;

import ru.shurl.storage.model.types.Account;
import ru.shurl.storage.model.entity.Link;

/**
 * @author vygulyarnyyao on 03.10.2021
 */
public interface StorageService {
    /**
     * Создает новую короткую ссылку по длинной
     * @param account   - пользователь
     * @param longUrl   - длинная ссылка
     * @return
     */
    Link createLink(Account account, String longUrl);

    /**
     * Удаляет ссылку
     * @param account   - пользователь
     * @param shortUrl  - короткая ссылка
     */
    void deleteLink(Account account, String shortUrl);
}
