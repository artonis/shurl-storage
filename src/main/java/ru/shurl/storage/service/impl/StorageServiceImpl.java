package ru.shurl.storage.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.shurl.storage.exceptions.AccessException;
import ru.shurl.storage.exceptions.BadParamException;
import ru.shurl.storage.model.entity.Link;
import ru.shurl.storage.model.types.Account;
import ru.shurl.storage.repository.LinkRepository;
import ru.shurl.storage.service.NotifyService;
import ru.shurl.storage.service.ShortUrlGenerator;
import ru.shurl.storage.service.StorageService;

import java.time.OffsetDateTime;

import static ru.shurl.storage.model.enums.MessageType.ADD;
import static ru.shurl.storage.model.enums.MessageType.DELETE;

/**
 *
 * Имплементация сервиса хранения ссылок
 *
 * @author vygulyarnyyao on 03.10.2021
 */
@Service
@RequiredArgsConstructor
public class StorageServiceImpl implements StorageService {

    /**
     * Генератор коротких ссылок
     */
    private final ShortUrlGenerator shortUrlGenerator;
    /**
     * Репозиторий связей ссылок
     */
    private final LinkRepository linkRepository;
    /**
     * Сервис для нотификации остальных сервисов об изменениях на стороне мастер системы
     */
    private final NotifyService notifyService;

    @Override
    public Link createLink(Account account, String longUrl) {
        final Link link = new Link();
        link.setCreateTime(OffsetDateTime.now());
        link.setAccountId(account.getAccountId());
        link.setLongUrl(longUrl);
        link.setShortUrl(shortUrlGenerator.createUrl(account.getUserType()));
        final Link savedLink = linkRepository.save(link);
        notifyService.sendMessage(ADD, savedLink.getLinkId(), savedLink);
        return savedLink;
    }

    @Override
    public void deleteLink(Account account, String shortUrl) {
        final Link link = linkRepository.findByShortUrl(shortUrl);
        if (link == null) {
            throw new BadParamException("Link not found");
        }
        if (link.getAccountId()!=account.getAccountId()) {
            throw new AccessException("You haven't access to this link");
        }
        linkRepository.delete(link);
        notifyService.sendMessage(DELETE, link.getLinkId(), link);
    }
}
