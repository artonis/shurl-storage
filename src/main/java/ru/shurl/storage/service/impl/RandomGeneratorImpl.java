package ru.shurl.storage.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.shurl.storage.config.AppConfiguration;
import ru.shurl.storage.exceptions.BadParamException;
import ru.shurl.storage.model.enums.UserType;
import ru.shurl.storage.service.ShortUrlGenerator;

import java.util.Random;

/**
 * Генератор короткой ссылки на базе случайного генератора
 *
 * @author vygulyarnyyao on 03.10.2021
 */
@Service
@RequiredArgsConstructor
public class RandomGeneratorImpl implements ShortUrlGenerator {

    private static final char[] AVAILABLE_SYMBOLS = new char[62];

    private final AppConfiguration appConfiguration;

    static {
        for (int i = 0; i < 26; i++) {
            AVAILABLE_SYMBOLS[i * 2] = (char)('a' + i);
            AVAILABLE_SYMBOLS[i * 2 + 1] = (char)('A' + i);
        }
        for (int i = 52; i < 62; i++) {
            AVAILABLE_SYMBOLS[i] = (char)((i - 52) + '0');
        }
    }

    @Override
    public String createUrl(UserType userType) {
        final int linkLength;
        switch (userType) {
            case FREE:
                linkLength = appConfiguration.getFreeLinkLength();
                break;
            case PREMIUM:
                linkLength = appConfiguration.getPremiumLinkLength();
                break;
            default:
                throw new BadParamException("Unknown type of user " + userType);
        }
        return generateLink(linkLength);
    }

    private String generateLink(int length) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            sb.append(AVAILABLE_SYMBOLS[random.nextInt(62)]);
        }
        return sb.toString();
    }
}
