package ru.shurl.storage.service;

import ru.shurl.storage.model.enums.UserType;

/**
 * Генератор уникальных коротких ссылок
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public interface ShortUrlGenerator {
    String createUrl(UserType userType);
}
