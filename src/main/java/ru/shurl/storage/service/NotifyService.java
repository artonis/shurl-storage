package ru.shurl.storage.service;

import ru.shurl.storage.model.entity.Link;
import ru.shurl.storage.model.enums.MessageType;

/**
 * @author vygulyarnyyao on 03.10.2021
 */
public interface NotifyService {

    void sendMessage(MessageType type, Long id, Link content);

}
