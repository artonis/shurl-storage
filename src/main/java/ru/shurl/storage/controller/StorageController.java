package ru.shurl.storage.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.shurl.storage.model.types.Account;
import ru.shurl.storage.model.entity.Link;
import ru.shurl.storage.service.StorageService;
import ru.shurl.storage.utils.JwtUtils;

import static ru.shurl.storage.utils.Constants.JWT_HEADER;

/**
 * Контроллеры для вызовов микросервиса хранения ссылок
 *
 * @author vygulyarnyyao on 03.10.2021
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/storage")
public class StorageController {

    private final StorageService storageService;

    @PostMapping
    public Link createLink(@RequestHeader(JWT_HEADER) String jwt, @RequestBody String longUrl) {
        final Account account = JwtUtils.getAccountByToken(jwt);
        return storageService.createLink(account, longUrl);
    }

    @DeleteMapping("/short")
    public void deleteLink(@RequestHeader(JWT_HEADER) String jwt, @RequestBody String shortUrl) {
        final Account account = JwtUtils.getAccountByToken(jwt);
        storageService.deleteLink(account, shortUrl);
    }
}
