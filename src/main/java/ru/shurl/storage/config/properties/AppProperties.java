package ru.shurl.storage.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author vygulyarnyyao on 05.10.2021
 */
@Data
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private int freeLinkLength;
    private int premiumLinkLength;
}
