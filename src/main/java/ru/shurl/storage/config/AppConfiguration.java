package ru.shurl.storage.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import ru.shurl.storage.config.properties.AppProperties;

/**
 * @author vygulyarnyyao on 03.10.2021
 */
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(AppProperties.class)
public class AppConfiguration {

    private final AppProperties appProperties;

    public int getFreeLinkLength() {
        return appProperties.getFreeLinkLength();
    }

    public int getPremiumLinkLength() {
        return appProperties.getPremiumLinkLength();
    }
}
