package ru.shurl.storage.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.shurl.storage.exceptions.AccessException;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class ResponseControllerAdvice {

    @ExceptionHandler(AccessException.class)
    public ResponseEntity<Void> handleAccessException(HttpServletRequest request, Exception e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}