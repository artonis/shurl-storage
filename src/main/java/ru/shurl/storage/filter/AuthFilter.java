package ru.shurl.storage.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.shurl.storage.utils.Constants.JWT_HEADER;

/**
 * @author vygulyarnyyao on 04.10.2021
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AuthFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        final String token = request.getHeader(JWT_HEADER);

        if (checkToken(token)) {
            doFilter(request, response, filterChain);
        } else {
            response.sendError(HttpStatus.FORBIDDEN.value(), "Access denied");
        }
    }

    private boolean checkToken(final String token) {
        return true;
        //TODO Тут чекнуть токен
    }
}