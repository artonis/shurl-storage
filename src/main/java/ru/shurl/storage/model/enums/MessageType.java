package ru.shurl.storage.model.enums;

/**
 * Типы сообщений для нотификации через топик
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public enum MessageType {
    ADD,
    DELETE
}
