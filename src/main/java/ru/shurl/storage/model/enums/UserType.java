package ru.shurl.storage.model.enums;

/**
 * Типы аккаунтов пользователей
 *
 * @author vygulyarnyyao on 03.10.2021
 */
public enum UserType {
    FREE,
    PREMIUM
}
