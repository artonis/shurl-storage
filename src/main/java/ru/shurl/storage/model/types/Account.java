package ru.shurl.storage.model.types;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.shurl.storage.model.enums.UserType;

/**
 * Информация о пользователе
 *
 * @author vygulyarnyyao on 03.10.2021
 */
@Getter
@RequiredArgsConstructor
public class Account {
    /**
     * Идентификатор аккаунта
     */
    private final long accountId;
    /**
     * Тип аккаунта
     */
    private final UserType userType;
}
