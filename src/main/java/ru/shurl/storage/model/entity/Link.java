package ru.shurl.storage.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;

/**
 * Информация по ссылке
 *
 * @author vygulyarnyyao on 03.10.2021
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "link")
public class Link {
    /**
     * Уникальный идентификатор линка
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "link_id")
    private Long linkId;
    /**
     * Уникальный идентификатор аккаунта
     */
    @Column(name = "account_id")
    private long accountId;
    /**
     * Короткая ссылка
     */
    @Column(name = "short_url")
    private String shortUrl;
    /**
     * Длинная ссылка
     */
    @Column(name = "long_url")
    private String longUrl;
    /**
     * Дата и время создания
     */
    @Column(name = "create_time")
    private OffsetDateTime createTime;
}