package ru.shurl.storage.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.shurl.storage.config.AppConfiguration;
import ru.shurl.storage.model.enums.UserType;
import ru.shurl.storage.service.impl.RandomGeneratorImpl;

/**
 * @author vygulyarnyyao on 05.10.2021
 */
public class RandomGeneratorTest {

    @Autowired
    private AppConfiguration appConfiguration;

    @Test
    public void testGenerator() {
        final RandomGeneratorImpl randomGenerator = new RandomGeneratorImpl(appConfiguration);
        final String url = randomGenerator.createUrl(UserType.FREE);
        Assertions.assertEquals(url.length(), 7);
    }
}
