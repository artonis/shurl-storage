FROM openjdk:11

COPY ./target/storage-0.0.1-SNAPSHOT.jar /home/storage-0.0.1-SNAPSHOT.jar

CMD ["java","-jar","/home/storage-0.0.1-SNAPSHOT.jar"]