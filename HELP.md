# Getting Started

### Commands to run

* mvn clean install
* docker build -t shurl/storage:latest .
* docker-compose up

### Test
* curl -H "Authorization: 123 FREE" -d "www.google.com" http://localhost:8081/api/v1/storage
